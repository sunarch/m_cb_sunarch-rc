# Sources List

- Debian Wiki: [SourcesList](https://wiki.debian.org/SourcesList)
- Debian MAN page for [deb822 - Debian RFC822 control data format](https://manpages.debian.org/bookworm/dpkg-dev/deb822.5.en.html)
- Ubuntu MAN page for [sources.list - List of configured APT data sources](https://manpages.ubuntu.com/manpages/oracular/man5/sources.list.5.html)
